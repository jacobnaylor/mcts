from MCTS import MonteCarlo
from tests.test_board import TestBoard

board = TestBoard(tree_depth=3, tree_width=3)
mcts = MonteCarlo(board, C=1.4)

mcts.get_play()
