import datetime
import random as random
from math import log, sqrt


def calc_ucb1(state_wins, state_plays, global_plays, z_score):
    """
    Calculate the UCB1 with a given z_score.
    :param state_wins: How many wins have resulted from game state.
    :param state_plays: How many times game state resulting from the considered move has been visited.
    :param global_plays: How many plays have been played from each of the alternative moves.
    :param z_score: A parameter that controls how wide the confidence intervals should be. A higher z_score results
        in more move exploration.  A lower score will focus more on the most promising moves.
    :return: The player number for the current player.
    """
    mean_wins = state_wins / state_plays
    ucb = mean_wins + (z_score * sqrt(2 * log(global_plays) / state_plays))
    return ucb


class MonteCarlo(object):
    def __init__(self, board, **kwargs):
        seconds = kwargs.get('time', 5)
        self.z_score = kwargs.get('z_score', 1.4)
        self.max_moves = kwargs.get('max_moves', 10000)
        self.game_history_path = kwargs.get('game_history_path', None)

        self.board = board
        self.game_state = [()]
        self.depth_searched = 0
        self.calculation_time = datetime.timedelta(seconds=seconds)
        self.game_history = self.get_game_history()

    def get_game_history(self):
        if self.game_history_path is None:
            return {}
        else:
            # TODO load an existing file with history
            return {}

    def get_next_move(self):
        # for current game state, get all legal next moves. Check if they've all been played. if so, calc best.
        # if not, randomly choose next one.  Continue until game is complete.  Then back-propagate the result.
        legal_moves = self.board.legal_plays(self.game_state)
        for move in legal_moves:
            # Check if move list has all been visited
            next_state = self.board.next_state(self.game_state, move)
            if next_state in self.game_history:
                continue
            else:
                # All legal moves are not explored
                # TODO sim_remaining_game
                return None
        else:
            # All legal moves are explored
            return self.select_best_explored_move(legal_moves)

    def select_best_explored_move(self, legal_moves):
        ucbs = {}

        # TODO Calculate the total plays for all legals moves
        global_plays = 0

        for move in legal_moves:
            next_state = self.board.next_state(self.game_state, move)
            wins = self.game_history[next_state]['wins']  # would this work? wins, plays = self.game_history[next_state]
            plays = self.game_history[next_state]['plays']
            ucbs[next_state] = calc_ucb1(wins, plays, global_plays, self.z_score)

        best_move = max(ucbs, key=ucbs.get)
        return best_move

    def sim_remaining_game(self):
        pass

    def back_propagate(self):
        pass

