import random as random


class TestBoard(object):
    """
    Board object with a mock-up of a game, for testing the MCTS Class.
    """
    def __init__(self, tree_depth, tree_width):
        self.tree_depth = tree_depth
        self.tree_width = tree_width

    def start(self):
        """
        Return a representation of the starting position of the game.
        :param source: The starting position
        :param destination: The destination towards which you wish to move your object.
        :return: An empty List for a game with no moves played.
        """
        return []

    def current_player(self, state):
        """
        Takes the game state and returns the current player's number.
        :param state: A game state to check.
        :return: The player number for the current player.
        """
        return 1

    def next_state(self, state, play):
        """
        Takes the game state, and the move to be applied.
        :param state: A game state to which to append a move.
        :param play: A move to append to the state.
        :return: Returns the new game state.
        """
        new_state = state + (play, )
        return new_state

    def legal_plays(self, state_history):
        """
        Takes a sequence of game states representing the full game history, and returns the full list of moves that
        are legal plays for the current player.
        :param state_history: The full game history.
        :return: Returns the full list of legal moves from the current state.
        """
        if len(state_history) >= self.tree_depth:
            return []
        else:
            return range(self.tree_width)

    def winner(self, state_history):
        """
        Takes a sequence of game states representing the full game history, and determines if the game is now won.
        If the game is now won, return the player number.  If the game is still ongoing, return zero.  If the game
        is tied, return a different distinct value, e.g. -1.
        :param state_history: The full game history.
        :return: Returns the current player if the game is won.  If still ongoing, returns 0.  If drawn, return -1.
        """
        print(state_history)
        if len(state_history) >= self.tree_depth:
            total = 0
            for move in state_history:
                total += move

            win_constant = .95 * total / (self.tree_width * self.tree_depth)

            if random.random() < win_constant:
                return self.current_player(state_history)
            else:
                return -1
        else:
            return 0
