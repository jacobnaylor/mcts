# Borrowed heavily from:
# https://jeffbradberry.com/posts/2015/09/intro-to-monte-carlo-tree-search/
# with some modifications of my own to fit to my use cases.
from __future__ import division
import datetime
from random import choice
from math import log, sqrt


class MonteCarlo(object):
    def __init__(self, board, **kwargs):
        self.board = board
        self.states = [()]
        self.max_moves = kwargs.get('max_moves', 10000)
        self.max_depth = 0
        seconds = kwargs.get('time', 2)
        self.calculation_time = datetime.timedelta(seconds=seconds)
        self.wins = {}
        self.plays = {}
        self.C = kwargs.get('C', 1.4)

    def update(self, state):
        self.states.append(state)

    def run_simulation(self):
        # A bit of an optimization here, so we have a local
        # variable lookup instead of an attribute access each loop.
        plays, wins = self.plays, self.wins

        visited_states = set()
        states_copy = self.states[:]
        state = states_copy[-1]
        player = self.board.current_player(state)
        moves_states = []

        expand = True
        for t in range(1, self.max_moves + 1):
            legal = self.board.legal_plays(states_copy)
            for move in legal:
                next_state = self.board.next_state(state, move)
                # next_player = self.board.current_player(next_state)
                moves_states.append((move, next_state))

            if all(plays.get((player, S)) for p, S in moves_states):
                # If we have stats on all of the legal moves here, use them.
                log_total = log(sum(plays[(player, S)] for p, S in moves_states))
                value, move, state = max(((wins[(player, S)] / plays[(player, S)])
                                          + self.C * sqrt(log_total / plays[(player, S)])
                                          , p, S) for p, S in moves_states)
            else:
                # Otherwise, just make an arbitrary decision.
                move, state = choice(moves_states)

            states_copy.append(state)

            # `player` here and below refers to the player
            # who moved into that particular state.
            if expand and (player, state) not in plays:
                expand = False
                plays[(player, state)] = 0
                wins[(player, state)] = 0
                if t > self.max_depth:
                    self.max_depth = t

            visited_states.add((player, state))

            player = self.board.current_player(state)
            winner = self.board.winner(state)
            if winner:
                break

        for player, state in visited_states:
            if (player, state) not in plays:
                continue
            plays[(player, state)] += 1
            if player == winner:
                wins[(player, state)] += 1

    def get_play(self):
        state = self.states[-1]
        player = self.board.current_player(state)
        legal = self.board.legal_plays(state)

        # Bail out early if there is no real choice to be made.
        if not legal:
            return
        if len(legal) == 1:
            return legal[0]

        games = 0
        begin = datetime.datetime.utcnow()
        while datetime.datetime.utcnow() - begin < self.calculation_time:
            self.run_simulation()
            games += 1

        moves_states = [(p, self.board.next_state(state, p)) for p in legal]

        # Display the number of calls of `run_simulation` and the
        # time elapsed.
        print(games, datetime.datetime.utcnow() - begin)

        # Pick the move with the highest percentage of wins.
        percent_wins, move = max(
            (self.wins.get((player, S), 0) /
             self.plays.get((player, S), 1),
             p)
            for p, S in moves_states
        )

        # Display the stats for each possible play.
        for x in sorted(
                ((100 * self.wins.get((player, S), 0) / self.plays.get((player, S), 1),
                  self.wins.get((player, S), 0),
                  self.plays.get((player, S), 0), p)
                 for p, S in moves_states),
                reverse=True
        ):
            print("{3}: {0:.2f}% ({1} / {2})".format(*x))

        print("Maximum depth searched:", self.max_depth)

        return move
